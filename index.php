<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>

<style>
p{
  text-indent:0px;
}
</style>
<?php

$free = GetFreePageLinks();
$fee = GetFeePageLinks();

$free_val = array_values($free);
?>

<style type="text/css">
ul
{
	list-style-type: none;
}
</style>

</head>

<body>
<div align="center">
<h1>THE RETURN OF JESUS CHRIST</h1>
<p style="font-size:24px;">Anonymous</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&copy; Copyright 1996-2007 by Two Riders Press LLC.<br/>Updated 7-10-12</p>
<p><a href="<?php echo $free_val[0]; ?>"><img src="images/continue.jpg" width="113" height="45" /></a></p>
<hr />
<p style="margin-bottom: 5px;">Table of Contents:</p>
<div style="width: 400px; text-align: left">
	<ul>
		<li><a href="intro.php">Introduction</a></li>
		<li>
			<ul>
				<li><a href="commit.php">Story</a></li>
				<li>
					<ul>
						<li><a href="photo1-2.php">Testimony Photo Rendering</a></li>
						<li><a href="photo3-4.php">Painting; "Harvest"</a></li>
						<li><a href="photo5-6.php">Bridge Photo</a></li>
					</ul>
				</li>
			</ul>
		</li>
		<li><a href="ghost.php">Cloud Photos</a></li>
	</ul>
</div>
</div>
</body>
</html>