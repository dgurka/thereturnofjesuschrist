<?php

session_start();

require("Statics.php");

define("FEE_PAGE_ONE", 1);
define("FEE_PAGE_TWO", 2);
define("FEE_PAGE_THREE", 4);

/* options */

define("FEE_PAGE_ONE_TITLE", "Photos");
define("FEE_PAGE_TWO_TITLE", "Residences");
define("FEE_PAGE_THREE_TITLE", "Poem");

if(true)
{
	define("PAYPAL_EMAIL", "96.02rev@gmail.com");
	define("PAYPAL_ENDPOINT", "https://www.paypal.com/cgi-bin/webscr");
}
else
{
	define("PAYPAL_EMAIL", "enable_1321993099_biz@gmail.com");
	define("PAYPAL_ENDPOINT", "https://www.sandbox.paypal.com/cgi-bin/webscr");
}

define("FEE_PAGE_TOTAL", FEE_PAGE_ONE | FEE_PAGE_TWO | FEE_PAGE_THREE);

function GetConnection()
{
	$conn = new mysqli("localhost", "karner47764", "VFofWvSYtR", "karner47764_mysql");

	if ($conn->connect_errno) {
	    printf("Connect failed: %s\n", $conn->connect_error);
	    exit();
	}

	return $conn;
}

/**
 * check that the user has the proper permissions
 * to view a particular page
 * 
 * @param int $id the id of the page.
 */
function CheckForFee($id)
{
	$good = true;

	if($good && !isset($_SESSION["fee_pages"]))
		$good = false;

	if($good && !in_array($id, $_SESSION["fee_pages"]))
		$good = false;

	if(!$good)
	{
		$_SESSION["tmp.checker.uri"] = $_SERVER['REQUEST_URI'];
		$loc = "enter_code.php?page_id={$id}";
		header("Location: $loc");
	}
}

function AddPageCodeToDb($code, $pages, $transid)
{
	$conn = GetConnection();

	$code = $conn->escape_string($code);
	$pages = $conn->escape_string($pages);
	$transid = $conn->escape_string($transid);

	$sqlstr = "INSERT INTO transactions (datetimestamp, transaction_id, code, pages_code) VALUES (NOW(), '$transid', '$code', '$pages')";

	$conn->query($sqlstr);
}

function GeneratePaymentForm($amount, $transid)
{
	$_transid = urlencode($transid);
	?>
<form action="<?php echo PAYPAL_ENDPOINT ?>" id="paymentForm" method="post">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<?php echo PAYPAL_EMAIL ?>">
<input type="hidden" name="item_name" value="Payment To returnofjesuschrist.org">
<input type="hidden" name="currency_code" value="USD">
<input type="hidden" name="amount" value="<?php echo $amount; ?>">
<input type="hidden" name="return" value="http://thereturnofjesuschrist.org/thanks.php">
<input type="hidden" name="cancel_return" value="http://thereturnofjesuschrist.org/">
<input type="hidden" name="item_number" value="<?php echo $_transid; ?>" />
</form>
<script type="text/javascript">
document.getElementById("paymentForm").submit();
</script>
	<?php
}

function GenerateCode()
{
	$options = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$code = "";

	for($i = 0; $i < 10; $i++)
	{
		$key = rand(0, strlen($options) - 1);
		$code .= $options[$key];
	}

	return $code;
}

function CheckCode($code)
{
	$conn = GetConnection();

	$_code = $conn->escape_string($code);

	$sqlstr = "SELECT * FROM transactions WHERE code = '$_code' AND payed = '1'";

	$result = $conn->query($sqlstr);

	$good = false;
	while($row = $result->fetch_assoc())
	{
		$good = true;
		if(!isset($_SESSION["fee_pages"]))
		{
			$_SESSION["fee_pages"] = array();
		}

		$_SESSION["fee_pages"][] = $row["pages_code"];
	}

	if(!$good)
		return false;

	return true;
}

function GetPageName($id)
{
	$id = (string)$id;
	switch ($id) {
		case FEE_PAGE_ONE:
			return FEE_PAGE_ONE_TITLE;
			break;

		case FEE_PAGE_TWO:
			return FEE_PAGE_TWO_TITLE;
			break;

		case FEE_PAGE_THREE:
			return FEE_PAGE_THREE_TITLE;
			break;
		
		default:
			return null;
			break;
	}
}

function GetFreePageLinks()
{
	$pages = array();

	$pages["Introduction"] = "intro.php";
	/*$pages["Commitment"] = "commit.php";
	$pages["Testimony"] = "testimony.php";
	$pages["Message"] = "message1.php";
	$pages["Compiling"] = "compiling1.php";*/
	$pages["Shop Group Photo Renderings"] = "photo1-2.php";
	$pages["Testimony Photo and Farm Scene"] = "photo3-4.php";
	$pages["Bridge Photos"] = "photo5-6.php";
	$pages["Cloud Photos"] = "ghost.php";
	//$pages["Biblical Passages"] = "biblicalpassages.php";
	$pages["Footnotes"] = "footnotes.php";

	return $pages;
}

function GetFeePageLinks()
{
	$pages = array();

	$pages["Residences"] = "residences.php";
	$pages["New Photos"] = "new_photos.php";
	$pages["Poem"] = "poem.php";

	return $pages;
}

function GetPageList()
{
	$pages = array();

	$pages[] = "intro.php";
	$pages[] = "commit.php";
	$pages[] = "testimony.php";
	$pages[] = "message1.php";
	$pages[] = "message2.php";
	$pages[] = "compiling1.php";
	$pages[] = "compiling2.php";
	$pages[] = "footnotes.php";
	$pages[] = "photo1-2.php";
	$pages[] = "photo3-4.php";
	$pages[] = "photo5-6.php";
	$pages[] = "ghost.php";
	//$pages[] = "biblicalpassages.php";

	/*// pay pages

	$pages[] = "paymentform.php";
	$pages[] = "residences.php";
	$pages[] = "new_photos.php";
	$pages[] = "poem.php";*/
	
	return $pages;
}

function OutputPageLinks($link_set)
{
	?>
<?php foreach ($link_set as $text => $link): ?>
	<a href="<?php echo $link; ?>"><?php echo $text; ?></a><br/>
<?php endforeach ?>
	<?php
}

function GetCurrentPageID($pages, $current)
{
	foreach ($pages as $key => $value) 
	{
		if($value == $current)
			return $key;
	}
	
	return null;
}

function GetPrevPage()
{
	$current = basename($_SERVER["SCRIPT_NAME"]);
	$pages = GetPageList();
	
	if(Statics::$_current === null)
	{
		Statics::$_current = GetCurrentPageID($pages, $current);
	}

	if(Statics::$_current === null)
		return "";
	else if(Statics::$_current == 0)
		echo "index.php";
	else
		echo $pages[Statics::$_current - 1];
}

function GetNextPage()
{
	$current = basename($_SERVER["SCRIPT_NAME"]);
	$pages = GetPageList();

	if(Statics::$_current === null)
	{
		Statics::$_current = GetCurrentPageID($pages, $current);
	}

	if(Statics::$_current === null)
		return "";

	else if(Statics::$_current + 1 == count($pages))
		echo "index.php";
	else
		echo $pages[Statics::$_current + 1];
}

function Tester()
{
	echo basename($_SERVER["SCRIPT_NAME"]) . PHP_EOL;
	var_dump($_SERVER);
}