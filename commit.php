<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
<h3>The Commitment</h3>
<p> In nineteen sixty-eight,  the subject would lie awake at night and see the faces (in his mind) of  schoolmates that were killed in the Vietnam war.  The above, combined with the lyrics from the  song &quot;Blowin' in the Wind&quot; (&quot;How many times can a man turn his head and pretend  that he just doesn't see?&quot;), initiated his commitment.</p>
<p>Some nights later, the subject  opened the door and supped with Jesus, during which he said to Him, &quot;If you  need a way to get through, go ahead and use me.&quot;  He surmised, what's it matter if one more  person is taken away?</p>
<p>Then came a night of stillness,  combined with anguish.  The subject  responded, &quot;Go ahead and put it on me.&quot;   Moments later, he saw a bright light, which was followed by a sense that  something left his body, exiting upward.</p>
<p>Months earlier this same sensation  occurred while beside his mother's deathbed.   He felt something leave her body at her death.  </p>
<p>Then came this recurring dream.  He parked his car, got out and went into the  rear entrance of a house.  The house was  unfurnished.  He then went down a hallway  and into a room containing nothing but a young, long–haired male sitting  cross-legged on the floor.</p>
<p>The subject was involved in a  budding relationship with a beautiful girl at the time of the commitment.  The relationship was strained by the social  changes of the era, and ended as a result of the commitment.  The commitment prevented him from sowing his  seed with her.  When the time came one  morning, he walked out into the yard instead and sat down in  and old abandoned foreign car, which was under a tree.  Meanwhile she had gone into the kitchen.  When he returned, she asked, &quot;You were out  there sitting in that old car?&quot;</p>
<p>One of the times she mentioned going  somewhere together, he responded with, &quot;But not on Tuesday afternoon.&quot;  At the time he didn't know why he said  that.  </p>
<p>They went to see only one movie (at  the end of the relationship), <em>Gone with the Wind.  </em>At that time, he was unaware of the  connotation to the song, &quot;Blowin' in the Wind.&quot;  </p>
</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
