<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
  <h3>Footnotes</h3>
  <p>The name Abraham is associated with the apartment where the ghost was seen.</p>
  <p>The person referred to as a saint, lived in the same city as  the discovered house of the 68 dream.</p>
  <p>The farm scene painting became very close to being tossed in  the trash.</p>
  <p>The subject had a sporadic twenty year psychosomatic aliment  that cleared-up when he realized in 96 what his life had been about.</p>
  <p>The young man “sitting crossed-legged on the floor” in the  68 dream connects to Chicago's 25 or 6 to 4. Also later the connection to the  Moody Blues, Tuesday Afternoon.</p>
  <p>The subject thought, no mount of olives where the seeing of  the ghost happened, only a spruce tree. While in the area of the apartment he  went by it and happened to meet its resident. They talked a few minutes, then  the resident in trying to make a point said, “Let's say you have some olives.”</p>
  <p>While the subject was talking with a middle school friend, a  person known much lesser to the subject joined in. The subject mentioned  ice-skating alone at night on a pond near his house. The lesser known person  said, “Come on over and skate with us.” He ended up as one of the mentioned  killed schoolmates. The subject did go skate with them.</p>
</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />

<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
