<?php require("funcs.php"); CheckForFee(FEE_PAGE_THREE); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
<style type="text/css">
p
{
	/* override the text indent and add a margin */
	text-indent: 0;
	margin-left: 20px;
}
</style>
</head>

<body>

<div class="box">

<p>After appearing at a door <br />
Years later there’s more <br />
I had doubts in store <br />
So he moved my core</p>

<p>Feeling empty and weak <br />
Denied want and got meek <br />
Viewed the work ahead <br />
Yearned to be elsewhere instead</p>

<p>A minister was in my morning thoughts <br />
Didn’t care about any ill wrought <br />
Kept pushing through a work stop <br />
Soon ahead Mt. Revelation’s top</p>

<p>I heard that minister talk <br />
I once showed him something <br />
He came back in a slow walk <br />
Saying, “See here this one thing”</p>

<p>I could see my hands still working <br />
My core had stepped aside <br />
One never knows what’s lurking <br />
What an astounding abide</p>

<p>It’s about what got used <br />
One can see it by paying dues <br />
It will give you good blues <br />
My, what great news</p>

<p>The ministers of dues paying people <br />
That had to stay nearer the steeple <br />
He seen it then showed me <br />
I see now how less my fee</p>

<p>Less doubt each day <br />
Then 02 took it away <br />
I can go there to abide <br />
Then note the shallow tied</p>

<p>Go there at the sound of might <br />
Then get restored by a flight <br />
Being long in a grip <br />
Then released at each trip</p>

<p>With many things built <br />
Is there existing a tilt? <br />
Now an invisible path <br />
As valid as math</p>

<p>If these words don’t reveal <br />
You’re getting a good deal <br />
It’s better to be foiled <br />
Than talk make one spoiled.</p>

</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
