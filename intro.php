<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
<h3>Introduction</h3>
<p>Verification  of the title results from boldness, scripture, and an inner voice.  In 1968 I stated to Christ, &quot;If you need a  way to get through, go ahead and use me.&quot;   When it was realized in 1996 what the above may have wrought, I had  doubts about my former boldness being proper.   After becoming aware of the scripture, &quot;Let us therefore come boldly  unto the throne of grace...,&quot; that doubt was removed.  Currently, it fits that if this works&quot;  seeding was a bold action, then its fruit is also, thus the title.  The title is biblically justified by; Jas.  2:17 mention of faith with works, the story was seeded in faith and produced  many works, from Rev. 1:7 &quot;Behold he cometh with clouds;...,&quot; from Mt.  24:26,&quot;...of that day and hour knoweth no man,&quot; I didn't know what the works  were previous to 1996 , and the lightening described in <a href="javascript:passage1()">Mt. 24:27</a> being similar  to the internet.  Before the taking of a  very interesting photo (1979) a voice in my head was heard.  While considering this current title, a voice  was heard saying, &quot;You had no fear of your fate after 1968.&quot;  Then weeks later came &quot;Fear not, it is I.&quot;</p>
<p>Between 1968 and 1983 four unusual  events took place involving who will be referred to in the story as the  subject.  The events in order of  occurrence are:  the sensation of something  leaving his body exiting upward, the finding of a house he had a dream about  years earlier, a photo with a cross over his and a suspected thief&quot;s head, and  the seeing of a ghost (Christ's) at a door.   In 1994 his curiosity was prodded by some lesser events which resulted  in a connection of the happenings into a story of:  Commitment, Testimony and Message.</p>
</div>
<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
