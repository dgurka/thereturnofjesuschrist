<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
  <p>Next to the door  was a table containing paints, brushes, and the uncompleted <a href="photo3-4.php#4">farm scene  painting</a>.  </p>
  <p>The tree and birds in the painting  are symbolic of travel.  As a child, the  subject once jumped for a tree branch that was well out of reach.  A friend said, &quot;Why are you trying when you  know you can’t reach it?&quot;  Tree branches  were in reach while sitting in the abandoned car.</p>
  <p>Years later, while waiting outside a  friend’s house for him to return, the subject climbed a tree.  When the friend arrived, the subject called  his name.  The friend asked.  &quot;What are you doing in the tree?&quot;  He in turn responded, &quot;I don’t know.&quot;</p>
  <p>Three of the many jobs the subject  had were with the tree companies.</p>
  <p> The birds complete the travel.  As a child, he wanted to be a bird when  grown.  A cynical downtrodden relative  had said to the subject a couple of time, &quot;Tell me what you’re going to be when  you grow up.&quot;</p>
  <p>While painting the picture, he was  totally unaware of any current-day significance or meaning it would have.</p>
  <p> The  picture ended up being boxed in storage for the next thirteen years.</p>
  <p>During the message phase, a stranger  approached the subject on the street and asked, &quot;Are you writing a book?&quot;</p>
  <p>The subject said, &quot;No.&quot;</p>
  <p>Then the stranger said, &quot;Well,  promise me you will, okay?&quot;</p>
  <p>The  response was, &quot;Okay, I promise.&quot;  The  subject sensed that the stranger was a fellow veteran.  During the compiling, he realized that the  stranger must have been influenced by the photographs.</p> </div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
