<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
  <p>The two men in suits, to the  subject’s right, indicate that it’s time to listen to Jesus.  It appears that there is something being  relayed from the one with his head turned to the next man in a suit.</p>
  <p>The fellow to the subject’s left  (under a cross) is suspected to have stolen something from the subject.  The subject sought him out to be in the  photo, as he was standing in the background appearing reluctant.</p>
  <p>The <a href="photo1-2.php#2">second photo</a> (all  three were taken at Christmas time) connotes a greeting.  The subject noticed that he had been using  the greeting &quot;howdy&quot;.  He had never used  &quot;howdy&quot; before, nor after, the testimony era.</p>
  <p> Possibly the photos are saying,  &quot;Listen, it’s now time to be cowboys and not soldiers.&quot; </p>
  <p>If  the third photo (not shown) is of any significance, it would be that it is  saying it’s time to move on again because the subject’s face is partially  blocked by two others.  He did move on  with the coming of spring.  This stay of  employment was his longest in thirty-six years of employment. </p>
  <p></p>
<h3>The Message</h3>
<p>Three years and two residence  changes later, the message phase took place.   These apartments were owned by Jewish people and the subject stayed about  one year.  </p>
<p>At about three in the morning, while  sitting on the floor and picking on a guitar in a dimly lit room, a ghostly  form appeared before the subject, dressed in a white robe.  He (or it) appeared at the apartment door.  This happened approximate to the passing of  the subject’s father.</p>
<p>&nbsp;</p>
</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
