<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
  <p>One of his  dominant childhood memories is of her telling him (as an arm-held child) to  look at the sparks created above the electrical streetcars.  This connoting that a spark from above (being  from God) can motivate people.</p>
  <p>She had taken him to see the movies, <em>High Noon</em> and <em>Bridge on the River Kwai</em>, both of which were later recognized  as being influential to the mission.</p>
  <p>The instilments manifested.  Sometimes when polishing his shoes, he would  show her the difference between a polished and an unpolished one.  She then referred to him as her &quot;before and  after&quot; boy.  Bewildered by these subtle  influences, he spend much of his childhood daydreaming.</p>
  <p>Because  of the suspected instilment, <a href="javascript:passage3();">1 Corinthians 9:16</a> struck a chord in the  subject during the compiling phase.  He  recognized that necessity had been laid upon him, thus making his life a  horizontal fall.</p>
  <p>The subject’s mother died on a  spring morning in 1968.  The evening  before (in spite of her delirium from the illness) she said to him, &quot;Tomorrow  you will be out of the service.&quot;  After  he said, &quot;Yes,&quot; he felt that she had gone into a state of rest and had no more  to say.  Looking back, he sensed that she  must have been satisfied that her work had not been in vain.</p>
  <p>Lastly, the subject has often  wondered (during his wandering) what the last thoughts were on the minds of the  faces envisioned in the nighttime of 1968.<br />
    Recently, at clock-out time, a tall  foreign house painter asked the subject in a loud voice, &quot;Are you going home?&quot;</p>
  <p>The subject responded, &quot;Yeah.&quot; </p>
</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
