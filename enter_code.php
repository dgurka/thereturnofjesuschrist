<?php

$pagename = "";
require_once("funcs.php");

$err = "";
$code = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$code = $_POST["code"];
	if(!CheckCode($code))
	{
		$err = "Code Invalid";
	}
	else
	{
		if(isset($_SESSION["tmp.checker.uri"]))
		{
			$loc = $_SESSION["tmp.checker.uri"];
			?>
			<script type="text/javascript">
				window.location = "<?php echo $loc; ?>";
			</script>
			<?php
		}
	}
}

if(isset($_GET["page_id"]))
{
	$pagename = GetPageName($_GET["page_id"]);
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">

<span style="color: red"><?php echo $err; ?></span>
<div>
	You've accessed "<?php echo $pagename; ?>" which is a fee page.<br/>
	Please enter your code below, or click <a href="paymentform.php">here</a> to pay the fee.
</div>

<div>
	<form method="post">
		Code: <input type="text" name="code" value="<?php echo $code; ?>" /> <input type="submit" value="Submit" />
	</form>
</div>

</div>

<div align="center"> <a href="conclusion.php">


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
