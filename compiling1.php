<?php require("funcs.php"); ?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
</head>

<body>

<div class="box">
<h3>Compiling</h3>
<p> In 1994, while at work on a  farmhouse, the subject’s attention kept getting directed to a stack of bushel  baskets.  He didn’t realize the  connection to <a href="javascript:passage2();">Mark 4:21</a> until 1996.</p>
<p>Some time later, he became moved on  hearing the song lyrics, &quot;Someone’s knocking at the door, someone’s ringing the  bell.  Do me a favor, open the door, let  him in.&quot;</p>
<p>On the job one day, a kid said to  the subject (referring to the job).   &quot;You’re wasting your time doing it that way.&quot;  The connotation of this statement tipped the  scale in favor of changing course toward the compiling phase.</p>
<p>The  subject before 1996 had been plagued with the feeling that no matter what he  was doing, he should be doing something else.   This feeling, combined with the above incidents, caused the compiling.</p>
<p>One day, the insight to the logic of  these encounters came about, that being E=MC<sup>2</sup>.  The energy given off in 1968 (what left the  subject’s body) re-manifested as a mass, that mass being the photos, paintings,  ghost vision, etc.</p>
<p>The story existed as a  catharsis.  To ease its emergence, the  subject used a tape recorder.  At the end  of the tapes he felt it necessary to mention what the background noise was.  The noise was caused by the strong wind  blowing through the trees.</p>
<p>The  most notable of the many events which came into the light (during this phase)  was that of the subject’s mother.  He  determined that she instilled the mission of Christ’s return in him at a very  young age.</p> </div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</div>

</body>
</html>
