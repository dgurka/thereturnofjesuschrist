<?php

require_once("funcs.php");

$err = "";

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$page1 = (isset($_POST["page_1"]) && $_POST["page_1"] = "yes");
	$page2 = (isset($_POST["page_2"]) && $_POST["page_2"] = "yes");
	$page3 = (isset($_POST["page_3"]) && $_POST["page_3"] = "yes");

	$transid = uniqid();

	$code = GenerateCode();

	if($page1)
		AddPageCodeToDb($code, FEE_PAGE_ONE, $transid);

	if($page2)
		AddPageCodeToDb($code, FEE_PAGE_TWO, $transid);

	if($page3)
		AddPageCodeToDb($code, FEE_PAGE_THREE, $transid);

	$price = 0;

	$price += ($page1) ? 2 : 0;
	$price += ($page2) ? 2 : 0;
	$price += ($page3) ? 2 : 0;

	$price = ($price == 6) ? 5 : $price;

	if($price)
	{
		GeneratePaymentForm($price, $transid);
	}
	else
	{
		$err = "Please select one or more pages.";
	}
}

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include("inc/header.inc") ?>
<title>Encounters with Christ</title>
<style type="text/css">
td
{
	margin-bottom: 8px;
	vertical-align: top;
}
</style>
</head>

<body>

<div class="box">
	<div>Purchasing all pages gives you a one dollar discount.</div><br/>
	<div>Sorry no refunds available</div><br/>
	<div style="color: red"><?php echo $err ?></div>
	<form method="post">
		<table>
			<tr>
				<td><?php echo FEE_PAGE_TWO_TITLE ?></td>
				<td><input type="checkbox" name="page_2" value="yes" /></td>
				<td width="80">Fee: $2.00</td>
				<td>Relates to two bible passages, gives an explanation for d&#233;j&#224; vu, is a guidance system, and gives logic to the seeing of a ghost.</td>
			</tr>
			<tr>
				<td><?php echo FEE_PAGE_THREE_TITLE ?></td>
				<td><input type="checkbox" name="page_3" value="yes" /></td>
				<td>Fee: $2.00</td>
				<td>Is a response to the mentioned very enlightening 2002 revelation.</td>
			</tr>
			<tr>
				<td><?php echo FEE_PAGE_ONE_TITLE ?></td>
				<td><input type="checkbox" name="page_1" value="yes" /></td>
				<td>Fee: $2.00</td>
				<td>Six from between 1976 and 2010.</td>
			</tr>
		</table>
		<input type="submit" value="Pay With Paypal" />
	</form>
</div>

<div align="center"> <a href="<?php GetPrevPage(); ?>"><img src="images/back.jpg" alt="Back" width="113" height="45" /></a>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
<a href="<?php GetNextPage(); ?>"><img src="images/continue.jpg" alt="Continue" width="113" height="45" /></a><br />


<a href="index.php"><img src="images/menu.jpg" alt="Continue" width="113" height="45" /></a>

</body>
</html>
